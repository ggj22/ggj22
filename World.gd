extends Node2D

const Player = preload("res://player.tscn")

onready var spawn1 = $spawn1
onready var spawn2 = $spawn2
onready var sentence = $sentence
onready var philController = $philosopherController
onready var winScreen = $winScreen

var player1
var player2

func _ready():
	player1 = Player.instance()
	player1.init("player2_jump",
			"player2_left",
			"player2_right",
			"player2_action",
			1)
	add_child(player1)
	player1.transform = spawn1.transform
	player1.connect("grabbedLetter", sentence, "onLetterGrabbed")
	
	player2 = Player.instance()
	player2.init("player1_jump",
			"player1_left",
			"player1_right",
			"player1_action",
			2)
	add_child(player2)
	player2.transform = spawn2.transform
	player2.connect("grabbedLetter", sentence, "onLetterGrabbed")
	
	sentence.connect("gameOver", self, "gameOver")
	
func gameOver(player1_points, player2_points):
	player1.deactivate()
	player2.deactivate()
	
	philController.deactivate()
	
	winScreen.displayScore(player1_points, player2_points)

