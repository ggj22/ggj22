extends Node2D

const INTERVAL = 4

var philosophers
var timeToNextTalk = 0
var active = true
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	philosophers = self.get_children()

func _process(delta):
	if !active:
		return
		
	timeToNextTalk -= delta;
	if timeToNextTalk < 0:
		makePhilosopherTalk()
		makePhilosopherTalk()
		setTimeToNextTalk()

func makePhilosopherTalk():
	var philosopher = pickIdlePhilosopher()
	if philosopher == null:
		return
		
	philosopher.onReadyToTalk(SentenceData.getLetter())
	
func setTimeToNextTalk():
	timeToNextTalk = INTERVAL

func pickIdlePhilosopher():
	philosophers.shuffle()
	for phil in philosophers:
		if phil.isIdle():
			return phil
			
func deactivate():
	active = false
	for phil in philosophers:
		phil.setIdle()
