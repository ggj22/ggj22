extends Label

onready var particles = $particles

var letterOwner = -1

func setLetterOwner(id):
	if id == 1:
		set("custom_colors/font_color", Color(0.8,0.6,0))
		particles.color = Color(0.8,0.6,0)
		
	elif id == 2:
		set("custom_colors/font_color", Color(0,0.4,0))
		particles.color = Color(0,0.4,0)
	
	particles.emitting = true
	letterOwner = id
	
func getLetterOwner():
	return letterOwner
