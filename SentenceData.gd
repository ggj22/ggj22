extends Node

const Difficulty = preload("res://Difficulty.gd")

const ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const SENTENCES = [
	"LOREM IPSUM DOLOR SIT AMET",
	"ARTA NON ERUBESCIT",
	"COGNOSCE TE IPSUM",
	"ERRARE HUMANUM EST",
	"FORTUNA CAECA EST",
	"LAPIS PHILOSOPHORUM",
	"NIL NISI BENE",
	"PER ASPERA AD ASTRA",
	"QUOD DISCIS TIBI DISCIS",
	"QUOT CAPITA TOT SENTENTIAE",
	"SIMILIS SIMILI GAUDET",
	"VENI VIDI VICI",
	"VENTIS VERBA DAS",
	"COGITO ERGO SUM",
	"CARPE DIEM",
	"MEMENTO MORI"
]

var difficulties = [
	createDifficulty(3,0.3),
	createDifficulty(4,0.25),
	createDifficulty(5,0.225),
	createDifficulty(6,0.2),
	createDifficulty(7,0.175),
	createDifficulty(8,0.15),
	createDifficulty(9,0.125),
	createDifficulty(10,0.1)
]

var sentenceNumOfLetters
var sentenceLetters
var randomLetters

func generateSentence():
	var sentence = SENTENCES[randi()%SENTENCES.size()]
	prepareSentenceLetters(sentence)
	prepareRandomLetters(sentence)
	return sentence

func prepareSentenceLetters(sentence):
	sentenceLetters = Array()
	for c in sentence:
		if c != " ":
			sentenceLetters.append(c)	
	sentenceLetters.shuffle()
	sentenceNumOfLetters = sentenceLetters.size()
	
func prepareRandomLetters(sentence):
	randomLetters = Array()
	for c in ALPHABET:
		if !c in sentence:
			randomLetters.append(c)
	randomLetters.shuffle()

func getLetter():
	sentenceLetters.shuffle()
	return sentenceLetters[0]

func getRandomLetter():
	randomLetters.shuffle()
	return randomLetters[0]

func acceptLetter(letter):
	var accepted = false
	
	if letter in sentenceLetters:
		accepted = true
		sentenceLetters.erase(letter)
		if !letter in sentenceLetters:
			randomLetters.append(letter)

	return accepted

func isComplete():
	return sentenceLetters.size() == 0

func getDifficulty():
	var completion = (sentenceNumOfLetters*1.0 - sentenceLetters.size())/sentenceNumOfLetters

	if completion < 0.1:
		return difficulties[0]
	elif completion < 0.2:
		return difficulties[1]
	elif completion < 0.3:
		return difficulties[2]
	elif completion < 0.45:
		return difficulties[3]
	elif completion < 0.6:
		return difficulties[4]
	elif completion < 0.85:
		return difficulties[5]
	else:
		return difficulties[6]
	
	var diff = Difficulty.new()
	diff.init(3,0.4)
	return diff

func createDifficulty(letters, interval):
	var diff = Difficulty.new()
	diff.init(letters, interval)
	return diff
