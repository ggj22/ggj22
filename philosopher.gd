extends Area2D

const PhilTalk = preload("res://phil_talk.tscn")

onready var letterText = $letterText
onready var talkContainer = $talkContainer
onready var sprite = $Sprite

var statue = preload("res://gfx/statue.png")
var statueLight = preload("res://gfx/statue-highlighted.png")

enum {IDLE, READY_TO_TALK, TALKING}
var state = IDLE
var letter
var talkingToPlayer
var readyToTalkTimeout

func _ready():
	setIdle()
	pass # Replace with function body.

func _process(delta):
	if state == READY_TO_TALK:
		readyToTalkTimeout -= delta
		if readyToTalkTimeout < 0:
			setIdle()
	
func setIdle():
	talkingToPlayer = -1
	letterText.visible = false
	state = IDLE
	sprite.set_texture(statue)
	for n in talkContainer.get_children():
		talkContainer.remove_child(n)
		n.queue_free()

func onReadyToTalk(letter):
	if state == READY_TO_TALK:
		return
		
	sprite.set_texture(statueLight)
	readyToTalkTimeout = 20
	talkingToPlayer = -1
	self.letter = letter
	state = READY_TO_TALK
	letterText.visible = true
	letterText.text = letter
	for n in talkContainer.get_children():
		talkContainer.remove_child(n)
		n.queue_free()
	
func startTalking(playerId):
	if state == IDLE || state == TALKING:
		return
	
	talkingToPlayer = playerId
	state = TALKING
	sprite.set_texture(statueLight)
	letterText.visible = false
	if talkContainer.get_child_count() == 0:
		var philTalk = PhilTalk.instance()
		talkContainer.add_child(philTalk)
		philTalk.init(self.letter)

func stopTalking():
	if state == IDLE || state == READY_TO_TALK:
		return
		
	var letter = talkContainer.get_child(0).getCurrentLetter()
	setIdle()
	return letter

func isIdle():
	return IDLE == state

func isTalking():
	return TALKING == state
	
func isTalkingToMe(playerId):
	return isTalking() && playerId == talkingToPlayer
	
func _onBodyEntered(body):
	if body is Player:
		body.addPhilosopherInRange(self)
		letterText.shake()

func _onBodyExited(body):
	if body is Player:
		body.removePhilosopher(self)
		if state == TALKING && body.playerId == talkingToPlayer:
			onReadyToTalk(letter)
		letterText.stopShake()
