extends Node2D

onready var welcomeSprite = $Sprite

func _process(delta):
	if Input.is_action_just_pressed("startGame"):
		if welcomeSprite.visible == false:
			get_tree().change_scene("res://World.tscn")
		else:
			welcomeSprite.visible = false
