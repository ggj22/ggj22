extends Node2D

const SentenceLetter = preload("res://sentence_letter.tscn")
onready var container = $HBoxContainer

var sentence

signal gameOver(blueScore, redScore)

func _ready():
	sentence = SentenceData.generateSentence()
	for c in sentence:
		var letter = SentenceLetter.instance()
		container.add_child(letter)
		letter.text = c

func onLetterGrabbed(playerId, letterGrabbed):
	for letter in container.get_children():
		if letter.getLetterOwner() != -1:
			continue
		
		if letter.text == letterGrabbed:
			letter.setLetterOwner(playerId)
			break
			
	if SentenceData.isComplete():
		emit_signal("gameOver", countPoints(1), countPoints(2))

func countPoints(playerId):
	var points = 0
	for letter in container.get_children():
		if playerId == letter.getLetterOwner():
			points+=1
	return points
