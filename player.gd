extends KinematicBody2D

class_name Player

var idleImage 
var leftImage
var rightImage
var jumpLeftImage
var jumpRightImage

var _jumpActionName
var _leftActionName
var _rightActionName
var _actionActionName
var playerId

const SPEED = 500
const JUMP_SPEED = -740
const GRAVITY = 32

var velocity = Vector2(0,0)
var philosophersInReach = Array()

onready var sprite = $animSprite
onready var jumpSprite = $jumpSprite
onready var anim = $AnimationPlayer

signal grabbedLetter(playerId, letter)

var active = true

func _ready():
	if playerId == 1:
		idleImage = preload("res://gfx/Player_B_idle.png")
		leftImage = preload("res://gfx/Player_B_Left.png")
		rightImage = preload("res://gfx/Player_B_Right.png")
		jumpLeftImage = preload("res://gfx/gracz B_skok w lewo.png")
		jumpRightImage = preload("res://gfx/gracz B_skok w prawo.png")
	elif playerId == 2:
		idleImage = preload("res://gfx/Player_A_idle.png")
		leftImage = preload("res://gfx/Player_A_Left.png")
		rightImage = preload("res://gfx/Player_A_Right.png")
		jumpLeftImage = preload("res://gfx/gracz A_skok w lewo.png")
		jumpRightImage = preload("res://gfx/gracz A_skok w prawo.png")
		
	sprite.set_texture(idleImage)

func init(jump, left, right, action, playerId):
	_jumpActionName = jump
	_leftActionName = left
	_rightActionName = right
	_actionActionName = action
	self.playerId = playerId
	

func _physics_process(delta):
	if !active:
		return 
		
	if Input.is_action_pressed(_rightActionName):
		velocity.x = lerp(velocity.x, SPEED, 0.3)
		sprite.set_texture(rightImage)
		if anim.current_animation != "walk":
			anim.stop()
			anim.play("walk")
	elif Input.is_action_pressed(_leftActionName):
		velocity.x = lerp(velocity.x, -SPEED, 0.3)
		sprite.set_texture(leftImage)
		if anim.current_animation != "walk":
			anim.stop()
			anim.play("walk")

	if Input.is_action_pressed(_jumpActionName) && is_on_floor():
		velocity.y = JUMP_SPEED
	else:
		velocity.y = velocity.y + GRAVITY
		
	velocity = move_and_slide(velocity, Vector2.UP)
	velocity.x = lerp(velocity.x, 0, 0.25)


	if is_on_floor():
		jumpSprite.visible = false
		sprite.visible = true
	else:
		if velocity.x < 0:
			jumpSprite.set_texture(jumpLeftImage)
		else:
			jumpSprite.set_texture(jumpRightImage)
		jumpSprite.visible = true
		sprite.visible = false
		

	if velocity.x < 1 && velocity.x > -1:
		sprite.set_texture(idleImage)
		if anim.current_animation != "idle":
			anim.stop()
			anim.play("idle")

	if Input.is_action_just_pressed(_actionActionName):
		for phil in philosophersInReach:
			doTheTalking(phil)

func addPhilosopherInRange(phil):
	philosophersInReach.append(phil)

func removePhilosopher(phil):
	philosophersInReach.erase(phil)

func doTheTalking(phil):
	if phil.isTalkingToMe(playerId):
		var letter = phil.stopTalking()
		if SentenceData.acceptLetter(letter):
			emit_signal("grabbedLetter", playerId, letter)
		
	elif !phil.isTalking():
		phil.startTalking(playerId)

func deactivate():
	active = false
