extends Node2D

onready var labelsContainer = $HBoxContainer
onready var sounds = $sounds


const LetterLabel = preload("res://letterLabel.tscn")
const MIN_LETTER_SWITCH_TIMEOUT = 0.1
const MAX_LETTER_SWITCH_TIMEOUT = 0.3

var active = false
var nextLetterSwitch = 0
var highlitedLetterId = 0
var interval
var soundPlaying

func _ready():
	playRandomSound()

func playRandomSound():
	soundPlaying = sounds.get_child(randi()%7)
	soundPlaying.play()
	yield(soundPlaying, "finished")
	playRandomSound()  
	
func init(letter):
	var difficulty = SentenceData.getDifficulty()
	interval = difficulty.interval
	var numberOfLetters = difficulty.numberOfLetters
	var targetLetter = randi()%numberOfLetters
	for i in numberOfLetters:
		var label = LetterLabel.instance()
		labelsContainer.add_child(label)
		if i == targetLetter:
			label.text = letter
		else:
			label.text = SentenceData.getRandomLetter()
		
func _process(delta):
	nextLetterSwitch -= delta
	if nextLetterSwitch < 0:
		switchLetter()
		nextLetterSwitch = interval

func getCurrentLetter():
	return labelsContainer.get_child(highlitedLetterId).text

func switchLetter():
	var nextLetter = highlitedLetterId + 1
	if nextLetter >= labelsContainer.get_child_count():
		nextLetter = 0
		
	labelsContainer.get_child(highlitedLetterId).set("custom_colors/font_color", Color(1,1,1))
	labelsContainer.get_child(nextLetter).set("custom_colors/font_color", Color(1,0.5,1))
		
	highlitedLetterId = nextLetter
