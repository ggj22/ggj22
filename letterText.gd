extends Label

var shake = false
var originalPosition

func _ready():
	originalPosition = rect_position

func _process(delta):
	if shake:
		rect_position.x = originalPosition.x + randf()*2-2
		rect_position.y = originalPosition.y + randf()*2-2

func shake():
	shake = true

func stopShake():
	shake = false
	rect_position = originalPosition
