extends Node2D

onready var labels = $VBoxContainer
onready var winnerLabel = $VBoxContainer/winner
onready var p1Score = $VBoxContainer/HBoxContainer/score1
onready var p2Score = $VBoxContainer/HBoxContainer/score2
onready var scoreAnim = $scoreAnim
onready var leftAnim = $leftAnim
onready var rightAnim = $rightAnim
onready var spaceAnim = $spaceAnim

onready var sprite1 = $Sprite
onready var sprite2 = $Sprite2

onready var leftWinText = $leftWinText
onready var leftWinLabel = $leftWinText/Label
onready var rightWinText = $rightWinText
onready var rightWinLabel = $rightWinText/Label

var finalTexts = [
"I think, therfore I am\n\nDescartes",
"We must reject this, both as standpoint and as method. If this be philosophy, then philosophy is a bubble floating in an atmosphere of unreality.\n\nMacmurray",
"Water is the first principle of everything.\n\nThales",
"A philosopher worthy of the name has never said more than a single thing[...]. And he has said only one thing because he has seen only one point...\n\nBergson",
"That atoms and the vacuum were the beginning of the universe; and that everything else existed only in opinion.\n\nDemocritus",
"The totality is not, as it were, a mere heap, but the whole is something besides the parts.\n\nAristotle"
]

func _ready():
	p1Score.set("custom_colors/font_color", Color(0.8,0.6,0))
	p2Score.set("custom_colors/font_color", Color(0,0.4,0))
	
func _process(delta):
	if visible && Input.is_action_just_pressed("startGame"):
		get_tree().reload_current_scene()	
	
func displayScore(p1_score, p2_score):
	finalTexts.shuffle()
	leftWinLabel.text = finalTexts[0]
	rightWinLabel.text = finalTexts[1]
	
	if(p1_score > p2_score):
		winnerLabel.text = "Yellow wins!"
		rightWinText.apply_scale(Vector2(0.75,0.75))
	elif(p2_score > p1_score):
		winnerLabel.text = "Green wins!"
		leftWinText.apply_scale(Vector2(0.75,0.75))
	else:
		winnerLabel.text = "A tie!"
	
	p1Score.text = str(p1_score)
	p2Score.text = str(p2_score)

	visible = true
	scoreAnim.play("results-slide-down")
	
	yield(get_tree().create_timer(0.5), "timeout")
	leftAnim.play("phil-left")
	sprite1.visible = true
	
	yield(get_tree().create_timer(0.5), "timeout")
	rightAnim.play("phil-right")
	sprite2.visible = true
	
	yield(get_tree().create_timer(2.0), "timeout")
	spaceAnim.play("space-slide-up")
